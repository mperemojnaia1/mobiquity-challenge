Packaging service
======

An API that helps to determine and pack items in a package by price and weight.
Accepts an absolute path to the file with Package details in UTF-8 format. The `pack("file.txt")` method returns the
 solution as a String.
 
**Algorithm**

The algorithm determines which things to put into the package so that the total weight is less than or equal to the
 package limit and the total cost is as large as possible.


Example of input:
----
```bash
81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)
8 : (1,15.3,€34)
75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)
56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)
```
Example of output:
----
```bash
4
-
2,7
8,9
```
Constraints:
----
1. Max weight that a package can take is = 100
2. There might be up to 15 items you need to choose from
3. Max weight and cost of an item is = 100
