package com.mobiquity.test.constants;

import static com.mobiquity.model.constants.PackageConstraintsConstants.MAX_ITEMS_NUMBER;
import static com.mobiquity.model.constants.PackageConstraintsConstants.MAX_VALUE;

public class CommonTestConstants {

    public static final String INVALID_INPUT_LINE = "Invalid input line format: ";
    public static final String INVALID_WEIGHT_FORMAT = "Invalid weight format: ";
    public static final String WEIGHT_IS_NOT_INDICATED = "Weight is not Indicated!";
    public static final String FILE_PATH_CANNOT_BE_EMPTY_OR_NULL = "File path cannot be empty or null!";
    public static final String TOO_MANY_ITEMS = "Number of items exceeds the maximum(" + MAX_ITEMS_NUMBER + "): ";
    public static final String EXCEEDS_MAX_VALUE = "Item weight exceeds the maximum number(" + MAX_VALUE + "): ";
    public static final String FILE_NOT_FOUND = "File not found: ";

    public static final String TEST_CASE1 = "/test_case1";
    public static final String TEST_CASE2 = "/test_case2";
    public static final String TEST_CASE3 = "/test_case3";
    public static final String TEST_CASE4 = "/test_case4";

    public static final String EXAMPLE_INPUT = "/example_input";
    public static final String INVALID_FILE_PATH = "/invalid_file_path";
    public static final String INVALID_WEIGHT_FORMAT_PATH = "/invalid_weight_format";
    public static final String INVALID_LINE_FORMAT_PATH = "/invalid_line_format";
    public static final String WEIGHT_IS_NOT_INDICATED_PATH = "/weight_is_not_indicated";
    public static final String TOO_MANY_ITEMS_PATH = "/too_many_items_input";
    public static final String WEIGHT_EXCEEDS_MAX_PATH = "/weight_exceeds_max";
}
