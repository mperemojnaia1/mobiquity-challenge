package com.mobiquity.service;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static com.mobiquity.test.constants.CommonTestConstants.EXCEEDS_MAX_VALUE;
import static com.mobiquity.test.constants.CommonTestConstants.TOO_MANY_ITEMS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.mobiquity.model.dto.PackageDetails;
import org.junit.jupiter.api.Test;

class ValidationServiceTest {

    public static final double WEIGHT = 120d;
    public static final long LIMIT_WEIGHT = 1000L;

    @Test
    void validateNumberOfItems() {
        var preparedPackageDetails = prepareListOfPackageDetails();
        var thrown = assertThrows(ValidationException.class, () ->
            ValidationService.validateNumberOfItems(LIMIT_WEIGHT, preparedPackageDetails));
        assertEquals(thrown.getMessage(), TOO_MANY_ITEMS + preparedPackageDetails.size());
    }

    @Test
    void validateValue() {
        var thrown = assertThrows(ValidationException.class, () -> ValidationService.validateValue(WEIGHT));
        assertEquals(thrown.getMessage(), EXCEEDS_MAX_VALUE + WEIGHT);
    }

    private List<PackageDetails> prepareListOfPackageDetails() {
        List<PackageDetails> packageDetails = new ArrayList<>();
        IntStream.range(0, 25).forEach( x->
            packageDetails.add(PackageDetails.builder()
                .weight((double) x)
                .build())
        );
        return packageDetails;
    }
}