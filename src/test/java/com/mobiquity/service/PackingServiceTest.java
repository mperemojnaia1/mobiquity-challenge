package com.mobiquity.service;

import javax.validation.ValidationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static com.mobiquity.model.constants.ParsingConstants.LINE_SEPARATOR;
import static com.mobiquity.test.constants.CommonTestConstants.EXAMPLE_INPUT;
import static com.mobiquity.test.constants.CommonTestConstants.EXCEEDS_MAX_VALUE;
import static com.mobiquity.test.constants.CommonTestConstants.FILE_NOT_FOUND;
import static com.mobiquity.test.constants.CommonTestConstants.FILE_PATH_CANNOT_BE_EMPTY_OR_NULL;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_FILE_PATH;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_INPUT_LINE;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_LINE_FORMAT_PATH;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_WEIGHT_FORMAT;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_WEIGHT_FORMAT_PATH;
import static com.mobiquity.test.constants.CommonTestConstants.TEST_CASE1;
import static com.mobiquity.test.constants.CommonTestConstants.TEST_CASE2;
import static com.mobiquity.test.constants.CommonTestConstants.TEST_CASE3;
import static com.mobiquity.test.constants.CommonTestConstants.TEST_CASE4;
import static com.mobiquity.test.constants.CommonTestConstants.TOO_MANY_ITEMS;
import static com.mobiquity.test.constants.CommonTestConstants.TOO_MANY_ITEMS_PATH;
import static com.mobiquity.test.constants.CommonTestConstants.WEIGHT_EXCEEDS_MAX_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.mobiquity.exception.APIException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class PackingServiceTest {

    public static final String EXAMPLE_OUTPUT = "src/test/resources/example_output";

    @Test
    public void getPackages() throws APIException, IOException {
        var result = PackingService.getPackages(EXAMPLE_INPUT);
        var expected = readFile(EXAMPLE_OUTPUT);
        assertFalse(result.isEmpty());
        assertEquals(result, expected);
    }

    @ParameterizedTest
    @MethodSource("getPackagesThrowsExceptionParameters")
    public void getPackagesThrowsException(
        String filePath,
        String errorMessage,
        Class<Throwable> exceptionClass
    ) {
        var thrown = assertThrows(exceptionClass, () -> PackingService.getPackages(filePath));
        assertTrue(thrown.getMessage().contains(errorMessage));
    }

    private static Stream<Arguments> getPackagesThrowsExceptionParameters() {
        return Stream.of(
            Arguments.arguments(
                INVALID_FILE_PATH,
                FILE_NOT_FOUND,
                APIException.class
            ),
            Arguments.arguments(
                null,
                FILE_PATH_CANNOT_BE_EMPTY_OR_NULL,
                APIException.class
            ),
            Arguments.arguments(
                INVALID_WEIGHT_FORMAT_PATH,
                INVALID_WEIGHT_FORMAT,
                APIException.class
            ),
            Arguments.arguments(
                INVALID_LINE_FORMAT_PATH,
                INVALID_INPUT_LINE,
                APIException.class
            ),
            Arguments.arguments(
                TOO_MANY_ITEMS_PATH,
                TOO_MANY_ITEMS,
                ValidationException.class
            ),
            Arguments.arguments(
                WEIGHT_EXCEEDS_MAX_PATH,
                EXCEEDS_MAX_VALUE,
                ValidationException.class
            )
        );
    }

    @ParameterizedTest
    @MethodSource("packSuccessfulParameters")
    public void getPackagesSuccess(
        String filePath,
        String expected
    ) throws APIException {
        var result = PackingService.getPackages(filePath);
        assertFalse(result.isEmpty());
        assertEquals(result, expected);
    }

    private static Stream<Arguments> packSuccessfulParameters() {
        return Stream.of(
            Arguments.arguments(
                TEST_CASE1,
                "4" + System.getProperty(LINE_SEPARATOR)
            ),
            Arguments.arguments(
                TEST_CASE2,
                "-" + System.getProperty(LINE_SEPARATOR)
            ),
            Arguments.arguments(
                TEST_CASE3,
                "2,7" + System.getProperty(LINE_SEPARATOR)
            ),
            Arguments.arguments(
                TEST_CASE4,
                "8,9" + System.getProperty(LINE_SEPARATOR)
            )
        );
    }

    private static String readFile(String path) throws IOException {
        return Files.readString(Path.of(path).toAbsolutePath());
    }
}