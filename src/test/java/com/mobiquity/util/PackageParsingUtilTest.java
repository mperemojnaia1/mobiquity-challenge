package com.mobiquity.util;

import java.io.IOException;
import java.util.stream.Stream;

import static com.mobiquity.test.constants.CommonTestConstants.FILE_NOT_FOUND;
import static com.mobiquity.test.constants.CommonTestConstants.FILE_PATH_CANNOT_BE_EMPTY_OR_NULL;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_FILE_PATH;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_INPUT_LINE;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_LINE_FORMAT_PATH;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_WEIGHT_FORMAT;
import static com.mobiquity.test.constants.CommonTestConstants.INVALID_WEIGHT_FORMAT_PATH;
import static com.mobiquity.test.constants.CommonTestConstants.WEIGHT_IS_NOT_INDICATED;
import static com.mobiquity.test.constants.CommonTestConstants.WEIGHT_IS_NOT_INDICATED_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.mobiquity.exception.APIException;
import com.mobiquity.test.constants.CommonTestConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PackageParsingUtilTest {

    @Test
    public void parseFileInput() throws APIException, IOException {
        var result = PackageParsingUtil.parseFileInput(CommonTestConstants.EXAMPLE_INPUT);
        assertFalse(result.isEmpty());
        assertEquals(result.size(), 4);
    }

    @ParameterizedTest
    @MethodSource("parseFileInputThrowsExceptionParameters")
    public void parseFileInputThrowsException(
        String filePath,
        String errorMessage
    ) {
        var thrown = assertThrows(APIException.class,
            () -> PackageParsingUtil.parseFileInput(filePath));
        assertTrue(thrown.getMessage().contains(errorMessage));
    }

    public static Stream<Arguments> parseFileInputThrowsExceptionParameters() {
        return Stream.of(
            Arguments.arguments(
                INVALID_FILE_PATH,
                FILE_NOT_FOUND
            ),
            Arguments.arguments(
                null,
                FILE_PATH_CANNOT_BE_EMPTY_OR_NULL
            ),
            Arguments.arguments(
                INVALID_WEIGHT_FORMAT_PATH,
                INVALID_WEIGHT_FORMAT
            ),
            Arguments.arguments(
                INVALID_LINE_FORMAT_PATH,
                INVALID_INPUT_LINE
            ),
            Arguments.arguments(
                WEIGHT_IS_NOT_INDICATED_PATH,
                WEIGHT_IS_NOT_INDICATED
            )
        );
    }
}