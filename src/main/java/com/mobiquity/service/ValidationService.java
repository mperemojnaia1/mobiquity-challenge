package com.mobiquity.service;

import javax.validation.ValidationException;
import java.util.List;

import static com.mobiquity.model.constants.PackageConstraintsConstants.MAX_ITEMS_NUMBER;
import static com.mobiquity.model.constants.PackageConstraintsConstants.MAX_VALUE;
import static com.mobiquity.model.enumeration.ErrorMessageEnum.EXCEEDS_MAX_NUMBER;
import static com.mobiquity.model.enumeration.ErrorMessageEnum.TOO_MANY_ITEMS;

import com.mobiquity.model.dto.PackageDetails;
import org.springframework.stereotype.Service;

/**
 * Validation Service - helps to validate the input data.
 */
@Service
public class ValidationService {

    /**
     * Validates that the number of items in the Package does not exceed the maximum amount.
     *
     * @param limitWeight - max weightn of a package.
     * @param list - list of package details objects.
     * @throws ValidationException when the number of items is too high.
     */
    public static void validateNumberOfItems(
        long limitWeight,
        List<PackageDetails> list
    ) {
        if (list.stream().mapToDouble(PackageDetails::getWeight).sum() <= (double) limitWeight
            && list.size() > MAX_ITEMS_NUMBER) {
            throw new ValidationException(TOO_MANY_ITEMS.getValue() + list.size());
        }
    }

    /**
     * Validates that the weight or price of the Package or item does not exceed the maximum amount.
     *
     * @param value - item weight or package max weight under validation.
     * @throws ValidationException when the number of items is too high.
     */
    public static void validateValue(double value) {
        if (value > MAX_VALUE) {
            throw new ValidationException(EXCEEDS_MAX_NUMBER.getValue() + value);
        }
    }
}
