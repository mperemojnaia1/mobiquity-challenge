package com.mobiquity.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.mobiquity.model.constants.PackageConstraintsConstants.MAX_ITEMS_NUMBER;
import static com.mobiquity.model.constants.PackageConstraintsConstants.MAX_VALUE;
import static com.mobiquity.model.constants.ParsingConstants.COMMA;
import static com.mobiquity.model.constants.ParsingConstants.DASH;
import static com.mobiquity.util.PackageParsingUtil.parseFileInput;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

import com.mobiquity.exception.APIException;
import com.mobiquity.model.constants.ParsingConstants;
import com.mobiquity.model.dto.Package;
import com.mobiquity.model.dto.PackageComparator;
import com.mobiquity.model.dto.PackageDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Packing Service - helps define the items that fit in the given package by weight and price.
 */
@Slf4j
@Service
public class PackingService {

    /**
     * Method to return a string of each set of items that fit into a package.
     *
     * @param filePath - the absolute path to the input file with package size and details.
     * @return - a string of each set of items that fit into a package.
     */
    public static String getPackages(String filePath) throws APIException {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            var pickedPackages = pickPackages(filePath);
            pickedPackages.forEach(pickedPackage -> stringBuilder.append(
                convertPackageInfoToStringBuilderResult(pickedPackage.getPackageDetailsList())));
            return stringBuilder.toString();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return stringBuilder.toString();
    }

    /**
     * Provides String builder result of each new row in the output string for each set of items that you put into a package.
     *
     * @param packageDetailsList - list of package items details.
     * @return a String builder result
     */
    private static StringBuilder convertPackageInfoToStringBuilderResult(
        List<PackageDetails> packageDetailsList
    ) {
        StringBuilder packageOutput = new StringBuilder();
        if (packageDetailsList != null) {
            IntStream.range(0, packageDetailsList.size()).forEach(i -> {
                PackageDetails packageDetails = packageDetailsList.get(i);
                packageOutput.append(packageDetails.getId());
                if (i + 1 < packageDetailsList.size()) {
                    packageOutput.append(COMMA);
                }
            });
        } else {
            packageOutput.append(DASH);
        }
        packageOutput.append(System.getProperty(ParsingConstants.LINE_SEPARATOR));
        log.debug(packageOutput.toString());
        return packageOutput;
    }

    /**
     * Pack and return the {@link #preparePackages(Long, List)} ()} algorithm result.
     *
     * @param filePath - the absolute path to the input file with package size and details.
     * @return a map of package capacity and the items that fit by weight and price.
     * @throws IOException, APIException
     */
    private static List<Package> pickPackages(
        String filePath
    ) throws IOException, APIException {
        var parsedPackagesList = parseFileInput(filePath);

        List<Package> preparedPackages = new ArrayList<>();
        parsedPackagesList.forEach(packages -> {
            Package chosenPackage = preparePackages(packages.getWeight(), packages.getPackageDetailsList());
            if (chosenPackage == null) {
                chosenPackage = Package.builder()
                    .weight(packages.getWeight())
                    .packageDetailsList(null)
                    .build();
            }
            preparedPackages.add(chosenPackage);
        });
        return preparedPackages;
    }

    /**
     * Algorithm to determine which things to put into the package so that the total weight is less than or equal to the package limit and the total
     * cost is as large as possible.
     * <p>
     * Iteration Strategy.
     *
     * @param limitWeight    - the maximum weight that the package can carry.
     * @param packageDetails - list of existing packages and items to pack.
     * @return an entry of prepared items for the package.
     */
    private static Package preparePackages(
        Long limitWeight,
        List<PackageDetails> packageDetails
    ) {
        //Validate if limitWeight is not higher then the max allowed amount and number of items if < 15
        ValidationService.validateValue(limitWeight);
        ValidationService.validateNumberOfItems(limitWeight, packageDetails);

        packageDetails = packageDetails.stream().filter(x -> x.getWeight() <= MAX_VALUE).collect(toList());

        Map<Package, List<PackageDetails>> matchCombinations = new HashMap<>();
        combineItems(limitWeight, matchCombinations, packageDetails);

        return matchCombinations.keySet().stream()
            .filter(x -> x.getCalculatedWeight() <= limitWeight)
            .filter(x -> x.getPackageDetailsList().size() <= MAX_ITEMS_NUMBER)
            .max(new PackageComparator())
            .orElse(null);
    }

    /**
     * Collects all matched items by price and weight in a map.
     *
     * @param limitWeight        - the maximum weight that the package can carry.
     * @param matchCombinations  - map of matched combinations of packages.
     * @param packageDetailsList - list of convenient package Details.
     */
    private static void combineItems(
        Long limitWeight,
        Map<Package, List<PackageDetails>> matchCombinations,
        List<PackageDetails> packageDetailsList
    ) {
        for (int i = 0; i < packageDetailsList.size(); i++) {
            var currentElement = packageDetailsList.get(i);
            addCurrentElementInMatchCombinations(limitWeight, matchCombinations, currentElement);
            List<PackageDetails> shortedList = getShortedListOfPackageDetails(packageDetailsList, i, currentElement);
            recursiveCombination(limitWeight, matchCombinations, singletonList(currentElement), shortedList);
        }
    }

    /**
     * A recursive call to combine all matched items by price and weight in a map in a second loop.
     *
     * @param limitWeight        - the maximum weight that the package can carry.
     * @param matchCombinations  - map of matched combinations of packages.
     * @param currentElements    - list of package details that are currently added to the list.
     * @param packageDetailsList - list of convenient package Details.
     */
    private static void recursiveCombination(
        Long limitWeight,
        Map<Package, List<PackageDetails>> matchCombinations,
        List<PackageDetails> currentElements,
        List<PackageDetails> packageDetailsList
    ) {
        if (packageDetailsList.isEmpty()) {
            return;
        }
        var currentWeight = currentElements.stream().mapToDouble(PackageDetails::getWeight).sum();
        var currentCost = currentElements.stream().mapToLong(PackageDetails::getCost).sum();

        //Loop through all package details and add to matched Combinations
        for (int i = 0; i < packageDetailsList.size(); i++) {
            if (limitWeight > currentWeight + packageDetailsList.get(i).getWeight()) {
                var combinationItem = Package.builder()
                    .weight(limitWeight)
                    .calculatedWeight(currentWeight + packageDetailsList.get(i).getWeight())
                    .calculatedCost(currentCost + packageDetailsList.get(i).getCost())
                    .packageDetailsList(Stream.concat(currentElements.stream(), Stream.of(packageDetailsList.get(i)))
                        .sorted(Comparator.comparingLong(PackageDetails::getId)).collect(toList()))
                    .build();
                matchCombinations.put(combinationItem, combinationItem.getPackageDetailsList());
                packageDetailsList.removeAll(combinationItem.getPackageDetailsList());
                recursiveCombination(limitWeight, matchCombinations, combinationItem.getPackageDetailsList(), packageDetailsList);
            }
        }
    }

    /**
     * Shortens the list by removing used iterations of package details.
     *
     * @param packageDetailsList - list of convenient package Details.
     * @param index              - current index of an iteration.
     * @param currentElement     - current element of package details which is being looped through.
     * @return a filtered list with used combinations.
     */
    private static List<PackageDetails> getShortedListOfPackageDetails(
        List<PackageDetails> packageDetailsList,
        int index,
        PackageDetails currentElement
    ) {
        List<PackageDetails> shortedList = new ArrayList<>();
        if (index == 0) {
            shortedList = packageDetailsList.stream().filter(x -> !x.equals(currentElement)).collect(toList());
        } else {
            List<PackageDetails> finalShortedList = shortedList;
            IntStream.range(index + 1, packageDetailsList.size()).forEach(x -> finalShortedList.add(packageDetailsList.get(x)));
        }
        return shortedList;
    }

    /**
     * Adds current element to the map if it matches the parameters.
     *
     * @param limitWeight       - the maximum weight that the package can carry.
     * @param matchCombinations - map of matched combinations of packages.
     * @param currentElement    - package details to be added to the map for one item combinations
     */
    private static void addCurrentElementInMatchCombinations(
        long limitWeight,
        Map<Package, List<PackageDetails>> matchCombinations,
        PackageDetails currentElement
    ) {
        matchCombinations.put(
            Package.builder()
                .weight(limitWeight)
                .calculatedWeight(currentElement.getWeight())
                .calculatedCost(currentElement.getCost())
                .packageDetailsList(singletonList(currentElement))
                .build(),
            singletonList(currentElement)
        );
    }
}
