package com.mobiquity.packer;

import com.mobiquity.exception.APIException;
import com.mobiquity.service.PackingService;
import lombok.extern.slf4j.Slf4j;

/**
 * Packer helps to pack the items you want to send so that the total weight is less than or equal to the package limit
 * and the total cost is as large as possible.
 */
@Slf4j
public class Packer {

    /**
     * Returns the Package with items that fit by the total weight and cost as a String.
     *
     * @param filePath - absolute path to the file with package details.
     * @return - returns the solution as a String.
     * @throws APIException if incorrect parameters are being passed.
     */
    public static String pack(String filePath) throws APIException {
        return PackingService.getPackages(filePath);
    }

}
