package com.mobiquity.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import static com.mobiquity.model.constants.ParsingConstants.BRACKETS_MATCHER;
import static com.mobiquity.model.constants.ParsingConstants.COLON;
import static com.mobiquity.model.constants.ParsingConstants.COMMA;
import static com.mobiquity.model.enumeration.ErrorMessageEnum.FILE_NOT_FOUND;
import static com.mobiquity.model.enumeration.ErrorMessageEnum.INVALID_FILE_PATH;
import static com.mobiquity.model.enumeration.ErrorMessageEnum.INVALID_INPUT_LINE;
import static com.mobiquity.model.enumeration.ErrorMessageEnum.INVALID_WEIGHT_FORMAT;
import static com.mobiquity.model.enumeration.ErrorMessageEnum.WEIGHT_IS_NOT_INDICATED;

import com.mobiquity.exception.APIException;
import com.mobiquity.model.dto.Package;
import com.mobiquity.model.dto.PackageDetails;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * Package parsing utility class to read a text file and transform the input into a list of {@link Package}.
 *
 * Accepts input: e.g. 81 : (1,53.38,€45) (2,88.62,€98)
 */
@UtilityClass
@Slf4j
public class PackageParsingUtil {

    //indexes
    private static final int ZERO_INDEX = 0;
    private static final int FIRST_INDEX = 1;
    private static final int SECOND_INDEX = 2;
    private static final int THIRD_INDEX = 3;

    /**
     * Parses a given file input into a list of {@link Package}.
     * 
     * @param filePath - absolute path to the file with package details.
     * @return - a list of parsed package weight and info.
     * @throws APIException, IOException
     * 
     * E.g. 81 : (1,53.38,€45) (2,88.62,€98)
     */
    public static List<Package> parseFileInput(
        final String filePath
    ) throws APIException, IOException {
        if (filePath == null) {
            throw new APIException(INVALID_FILE_PATH.getValue());
        }
        var inputStream = openInputStream(filePath);
        var scanner = new Scanner(inputStream);

        List<Package> packages = new ArrayList<>();
        while (scanner.hasNextLine()) {
            var line = scanner.nextLine();
            var parsedValues = line.split(COLON);
            if (parsedValues.length > FIRST_INDEX) {
                if (parsedValues[ZERO_INDEX] == null) {
                    log.error(WEIGHT_IS_NOT_INDICATED.getValue());
                }
                var weight = getWeight(parsedValues[ZERO_INDEX]);
                var parsedPackages = getPackages(parsedValues[FIRST_INDEX]);
                packages.add(Package.builder().weight(weight).packageDetailsList(parsedPackages).build());
            } else {
                log.error(INVALID_INPUT_LINE.getValue() + line);
                throw new APIException(INVALID_INPUT_LINE.getValue());
            }
        }
        inputStream.close();
        return packages;
    }

    /**
     * Parse package details and build a list of PackageDetails objects.
     *
     * @param packagesString - the string with the packages details from the input file
     * @return a list of parsed packages
     */
    private static List<PackageDetails> getPackages(String packagesString) {
        ArrayList<PackageDetails> packages = new ArrayList<>();
        
        //regex to match the values in the brackets
        var pattern = Pattern.compile(BRACKETS_MATCHER);
        var matcher = pattern.matcher(packagesString);
        while (matcher.find()) {
            var matchedString = matcher.group().subSequence(FIRST_INDEX, matcher.group().length() - FIRST_INDEX).toString();
            var parsedValues = matchedString.split(COMMA);
            var index = Integer.parseInt(parsedValues[ZERO_INDEX]);
            var weight = Double.parseDouble(parsedValues[FIRST_INDEX]);

            // Parse the costs with euro sign
            int cost;
            if (StringUtils.isNumeric(parsedValues[SECOND_INDEX].substring(FIRST_INDEX))) {
                cost = Integer.parseInt(parsedValues[SECOND_INDEX].substring(FIRST_INDEX));
            } else {
                cost = Integer.parseInt(parsedValues[SECOND_INDEX].substring(THIRD_INDEX));
            }

            var packageRequest = PackageDetails.builder()
                .id((long) index)
                .cost((long) cost)
                .weight(weight)
                .build();
            packages.add(packageRequest);
        }
        return packages;
    }

    /**
     * Parse the long value from a String.
     *
     * @param value - String value from the input file.
     * @return Long value.
     * @throws APIException - thrown when weight is not declared, or the format is wrong.
     */
    private static Long getWeight(String value) throws APIException {
        var trimmedStringValue = value.trim();

        if (StringUtils.isEmpty(trimmedStringValue)) {
            log.error(WEIGHT_IS_NOT_INDICATED.getValue());
            throw new APIException(WEIGHT_IS_NOT_INDICATED.getValue());
        }

        long weight;
        try {
            weight = Long.parseLong(trimmedStringValue);
        } catch (NumberFormatException exception) {
            log.error(INVALID_WEIGHT_FORMAT.getValue() + exception.getMessage());
            throw new APIException(INVALID_WEIGHT_FORMAT.getValue() + exception.getMessage());
        }
        return weight;
    }

    /**
     * Reads the given file and opens an InputStream.
     * 
     * @param filePath - path to the input file
     * @return an Open Input Stream
     * @throws APIException when file not found.
     */
    private InputStream openInputStream(String filePath) throws APIException {
        var inputStream = PackageParsingUtil.class.getClassLoader().getResourceAsStream(filePath);
        if (inputStream == null) {
            inputStream = PackageParsingUtil.class.getResourceAsStream(filePath);
            if (inputStream == null) {
                inputStream = ClassLoader.getSystemResourceAsStream(filePath);
            }
        }
        if (inputStream == null) {
            throw new APIException(FILE_NOT_FOUND.getValue() + filePath);
        }
        return inputStream;
    }
}

