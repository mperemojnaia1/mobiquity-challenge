package com.mobiquity.model.constants;

/**
 * Parsing constants to transform the input line into a POJO.
 */
public final class ParsingConstants {

    public static final String LINE_SEPARATOR = "line.separator";
    public static final String DASH = "-";
    public static final String COLON = ":";
    public static final String COMMA = ",";
    public static final String BRACKETS_MATCHER = "\\(.*?\\)";

}
