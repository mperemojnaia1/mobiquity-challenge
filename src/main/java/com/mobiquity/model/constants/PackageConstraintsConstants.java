package com.mobiquity.model.constants;

/**
 *  Additional constraints:
 *  - Weight and cost of an item must not be > 100
 *  - A package can carry up to 15 items
 */
public final class PackageConstraintsConstants {

    public static final long MAX_VALUE = 100L;
    public static final int MAX_ITEMS_NUMBER = 15;

}
