package com.mobiquity.model.enumeration;

import static com.mobiquity.model.constants.PackageConstraintsConstants.MAX_ITEMS_NUMBER;
import static com.mobiquity.model.constants.PackageConstraintsConstants.MAX_VALUE;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Error Messages Values.
 */
@AllArgsConstructor
public enum ErrorMessageEnum {

    INVALID_WEIGHT_FORMAT("Invalid weight format: "),
    WEIGHT_IS_NOT_INDICATED("Weight is not Indicated!"),
    TOO_MANY_ITEMS("Number of items exceeds the maximum(" + MAX_ITEMS_NUMBER + "): "),
    INVALID_INPUT_LINE("Invalid input line format: "),
    INVALID_FILE_PATH("File path cannot be empty or null!"),
    FILE_NOT_FOUND("File not found: "),
    EXCEEDS_MAX_NUMBER("Item weight exceeds the maximum number(" + MAX_VALUE + "): ");

    @Getter
    private final String value;

}
