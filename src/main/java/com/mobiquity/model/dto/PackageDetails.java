package com.mobiquity.model.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Package details
 */
@Data
@Builder
public class PackageDetails {

    private final Long id;
    private final Double weight;
    private final Long cost;

}
