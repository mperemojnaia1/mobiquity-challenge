package com.mobiquity.model.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;

/**
 * A combination of Package max weight and Package details.
 */
@Data
@Builder
public class Package {

    private final Long weight;
    private final Double calculatedWeight;
    private final Long calculatedCost;
    private final List<PackageDetails> packageDetailsList;
}
