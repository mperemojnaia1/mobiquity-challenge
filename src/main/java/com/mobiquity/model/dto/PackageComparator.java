package com.mobiquity.model.dto;

import java.util.Comparator;

/**
 * Package Comparator: to sort the list of Packages by cost and weight.
 */
public class PackageComparator implements Comparator<Package> {

    @Override
    public int compare(Package package1, Package package2) {
        var result = package1.getCalculatedCost().compareTo(package2.getCalculatedCost());
        if (result == 0) {
            return package2.getCalculatedWeight().compareTo(package1.getCalculatedWeight());
        } else {
            return result;
        }
    }
}
